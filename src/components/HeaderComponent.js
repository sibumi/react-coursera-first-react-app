import React, { Component } from 'react';
import { Navbar, NavbarBrand, Jumbotron, Nav, NavbarToggler, NavItem, Collapse } from 'reactstrap';
import { NavLink } from 'react-router-dom';


class Header extends Component {

    constructor(props){
        super(props);
        this.state ={
            isNavOpen : false,
        }
        this.toggleNav = this.toggleNav.bind(this)
    }

    toggleNav(){
        this.setState({
            isNavOpen : !this.state.isNavOpen,
        });
    }
    render() {
        return (

            <div>
                <Navbar dark expand="md">
                    <div className="container">
                         <NavbarToggler className='mr-2' onClick={this.toggleNav}/>
                        <NavbarBrand className='mr-auto' href='/'>
                            <img src='./assets/images/logo.png' height='30' width='41' alt='Ristorante Con Fusion'></img>
                        </NavbarBrand>
                       
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                        <Nav navbar>
                            <NavItem>
                                <NavLink className="nav-link" to='/home'><span className='fa fa-home fa-lg mr-1'></span>Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/aboutus'><span className='fa fa-info fa-lg mr-1'></span>About</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/menu'><span className='fa fa-list fa-lg mr-1'></span>Menu</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/contactus'><span className='fa fa-address-card fa-lg mr-1'></span>Contact</NavLink>
                            </NavItem>
                                  
                        </Nav>
                        </Collapse>
                    </div>
                </Navbar>

                <Jumbotron>
                    <div className='container'>
                        <div className='row row-header'>
                            <div className='col-12 col-sm-6'>
                                <h1>
                                    Ristorante Con Fusion
                                </h1>
                                <p>We take inspirations from the World's best cuisines and create a unique fusion experience. Our lip smacking creations will tickle your culinary senses!</p>

                            </div>

                        </div>
                    </div>
                </Jumbotron>

            </div>

        );

    }
}

export default Header;