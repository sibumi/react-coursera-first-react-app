import React from 'react';
import { Card, CardBody, CardImg, CardTitle, CardText, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import {Link} from 'react-router-dom'




function RenderDish({ dish }) {
    return (
        <div className="col-12 col-md-5 m-1">
            <Card>
                <CardImg width="100%" src={dish.image} alt={dish.title} />
                <CardBody>
                    <CardTitle>{dish.title}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        </div>
    );

}

function RenderComments({ comments }) {
    if (comments != null) {
        const myComments = comments.map((comment) => {
            return (
                <li key={comment.id}>
                    <p>{comment.comment}</p>
                    <p>-- {comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(comment.date)))} </p>
                </li>
            );
        });

        return myComments;
    } else {
        return <div></div>
    }

}


const Dishdetail = (props) => {
    if (props.dish != null) {
        return (
            <div className="container">
                <div className='row'>
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to='/menu'>Menu</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem active>
                                <Link to='/menu'>{props.dish.name}</Link>
                            </BreadcrumbItem>
                        </Breadcrumb>
                        <div className='col-12'>
                            <h3>{props.dish.name}</h3>
                            <hr />
                        </div>

                    </div>
                <div className="row">
                    <RenderDish dish={props.dish} />

                    <div className="col-12 col-md-5 m-1">
                        <h4>Comments</h4>
                        <ul><RenderComments comments={props.comments} /></ul>
                    </div>
                </div>
            </div>
        );
    } else return (
        <div className="container"></div>

    );

}











export default Dishdetail;